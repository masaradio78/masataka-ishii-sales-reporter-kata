﻿using NFluent;
using OurSalesReporterKata.Domain.Model;
using Xunit;

namespace OurSalesReporterKata.Tests.Domain.Model
{
    public class ReportDataTest
    {
        [Fact]
        void when_all_params_are_present_in_constructor_should_calculate_average_of_amount_sale()
        {
            var numberSales = 5;
            var sumOfItems = 11;
            var totalSalesAmount = 1441.84;
            var reportData = new ReportData(numberSales, sumOfItems, totalSalesAmount);
            
            Check.That(reportData.AverageAmountSale).Equals(288.37);
        }

        [Fact]
        void when_all_params_are_present_in_constructor_should_calculate_average_of_item_price()
        {
            var numberSales = 5;
            var sumOfItems = 11;
            var totalSalesAmount = 1441.84;
            var reportData = new ReportData(numberSales, sumOfItems, totalSalesAmount);

            Check.That(reportData.AverageItemPrice).Equals(131.08);
        }
    }
}