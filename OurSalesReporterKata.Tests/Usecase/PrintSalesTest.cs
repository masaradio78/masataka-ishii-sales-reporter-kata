﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Parser;
using OurSalesReporterKata.Domain.Printer;
using OurSalesReporterKata.Usecase;
using Xunit;

namespace OurSalesReporterKata.Tests.Usecase
{
    public class PrintSalesTest
    {
        private readonly Mock<IParserOrder> _mockParserOrder;
        private readonly Mock<ISalesPrinter> _mockSalesPrinter;
        private readonly PrintSales _printSales;

        public PrintSalesTest()
        {
            _mockParserOrder = new Mock<IParserOrder>();
            _mockSalesPrinter = new Mock<ISalesPrinter>();
            _printSales = new PrintSales(_mockParserOrder.Object, _mockSalesPrinter.Object);
        }

        [Fact]
        void should_parse_orders_with_orders_content()
        {
            string[] lines =
            {
                "orderid,userName,numberOfItems,totalOfBasket,dateOfBuy",
                "1, peter, 3, 123.00, 2021-11-30",
                "2, paul, 1, 433.50, 2021-12-11",
            };

            _printSales.Execute(lines);

            var contentLines = lines.Skip(1);
            _mockParserOrder.Verify(parserOrder => parserOrder.ParseOrders(contentLines));
        }

        [Fact]
        void should_print_sales_with_headers_and_orders()
        {
            string[] lines =
            {
                "orderid,userName,numberOfItems,totalOfBasket,dateOfBuy",
                "1, peter, 3, 123.00, 2021-11-30",
                "2, paul, 1, 433.50, 2021-12-11",
            };

            var orders = new List<Order>
            {
               new(1, "peter", 3, 123.0, DateTime.Parse("2021-11-30")),
               new(2, "paul", 1, 433.50, DateTime.Parse("2021-12-11"))
            };
            var contentLines = lines.Skip(1);
            _mockParserOrder.Setup(parserOrder => parserOrder.ParseOrders(contentLines)).Returns(orders);

            _printSales.Execute(lines);
            var listHeaders = new List<string>
            {
                "orderid", "userName", "numberOfItems", "totalOfBasket", "dateOfBuy"
            };
            _mockSalesPrinter.Verify(salesPrinter => salesPrinter.Print(orders, listHeaders));
        }
    }
}