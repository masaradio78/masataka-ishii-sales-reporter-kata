﻿using System;
using System.Collections.Generic;
using Moq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Parser;
using OurSalesReporterKata.Domain.Printer;
using OurSalesReporterKata.Usecase;
using Xunit;

namespace OurSalesReporterKata.Tests.Usecase
{
    public class PrintReportTest
    {
        private readonly Mock<IReportPrinter> _mockReportPrinter;
        private readonly Mock<IParserOrder> _mockParserOrder;

        private readonly PrintReport _printReport;

        public PrintReportTest()
        {
            _mockReportPrinter = new Mock<IReportPrinter>();
            _mockParserOrder = new Mock<IParserOrder>();
            _printReport = new PrintReport(_mockReportPrinter.Object, _mockParserOrder.Object);
        }

        [Fact]
        void when_order_lines_parse_to_list_order_should_print_report_with_report_data()
        {
            string[] lines =
            {
                "orderid,userName,numberOfItems,totalOfBasket,dateOfBuy",
                "1, peter, 3, 123.00, 2021-11-30",
                "2, paul, 1, 433.50, 2021-12-11",
            };
        
            var orders = new List<Order>
            {
                new(1, "peter", 3, 123.0, DateTime.Parse("2021-11-30")),
                new(2, "paul", 1, 433.50, DateTime.Parse("2021-12-11"))
            };
        
            _mockParserOrder.Setup(parserOrder => parserOrder.ParseOrders(It.IsAny<IEnumerable<string>>()))
                .Returns(orders);
            
            _printReport.Execute(lines);

            var expectedReportData = new ReportData(2, 4, 123.0 + 433.5);
            
            _mockReportPrinter.Verify(reportPrinter => reportPrinter.Print(expectedReportData), Times.Once);
        }
    }
}