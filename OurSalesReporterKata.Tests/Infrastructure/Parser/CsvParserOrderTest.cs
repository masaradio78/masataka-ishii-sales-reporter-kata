﻿using System;
using System.Collections.Generic;
using NFluent;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Infrastructure.Parser;
using Xunit;

namespace OurSalesReporterKata.Tests.Infrastructure.Parser
{
    public class CsvParserOrderTest
    {
        private readonly CsvParserOrder _csvParserOrder;

        public CsvParserOrderTest()
        {
            _csvParserOrder = new CsvParserOrder();
        }

        [Fact]
        void when_lines_contain_one_line_should_parse_one_order()
        {
            string[] lines = { "1, peter, 3, 123.00, 2021-11-30" };
            var result = _csvParserOrder.ParseOrders(lines);

            var expected = new List<Order> { new(1, "peter", 3, 123.0, new DateTime(2021, 11, 30)) };


            Check.That(result).ContainsExactly(expected);
        }

        [Fact]
        void when_lines_contain_three_lines_should_parse_three_orders()
        {
            string[] lines =
            {
                "1, peter, 3, 123.00, 2021-11-30",
                "2, paul, 1, 433.50, 2021-12-11",
                "4, john, 5, 467.35, 2021-12-30"
            };
            var result = _csvParserOrder.ParseOrders(lines);

            var expected = new List<Order>
            {
                new(1, "peter", 3, 123.0, new DateTime(2021, 11, 30)),
                new(2, "paul", 1, 433.50, new DateTime(2021, 12, 11)),
                new(4, "john", 5, 467.35, new DateTime(2021, 12, 30))
            };
            
            Check.That(result).ContainsExactly(expected);
        }
    }
}