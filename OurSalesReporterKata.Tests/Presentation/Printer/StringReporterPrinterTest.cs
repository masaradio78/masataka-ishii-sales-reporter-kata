﻿using Moq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Output;
using OurSalesReporterKata.Presentation.Printer;
using Xunit;

namespace OurSalesReporterKata.Tests.Presentation.Printer
{
    public class StringReporterPrinterTest
    {
        private readonly StringReportPrinter _stringReportPrinter;
        private readonly Mock<IOutput> _mockOutput;

        public StringReporterPrinterTest()
        {
            _mockOutput = new Mock<IOutput>();
            _stringReportPrinter = new StringReportPrinter(_mockOutput.Object);
        }

        [Fact]
        void Print_should_print_all_report_data_values_in_appropriate_output()
        {
            const int numberSales = 5;
            const int sumOfItems = 11;
            const double totalSalesAmount = 1441.84;
            var reportData = new ReportData(numberSales, sumOfItems, totalSalesAmount);

            _stringReportPrinter.Print(reportData);
            
            var expectedOutput = @$"+---------------------------------------------+
|                Number of sales |          5 |
|              Number of clients |          0 |
|               Total items sold |         11 |
|             Total sales amount |    1441.84 |
|            Average amount/sale |     288.37 |
|             Average item price |     131.08 |
+---------------------------------------------+";
            _mockOutput.Verify(output => output.Out(expectedOutput));
        }
    }
}