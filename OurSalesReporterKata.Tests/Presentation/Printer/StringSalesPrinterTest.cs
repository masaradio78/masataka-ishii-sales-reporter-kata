﻿using System;
using System.Collections.Generic;
using Moq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Output;
using OurSalesReporterKata.Presentation.Printer;
using Xunit;

namespace OurSalesReporterKata.Tests.Presentation.Printer
{
    public class StringSalesPrinterTest
    {
        private readonly StringSalesPrinter _stringSalesPrinter;

        private readonly Mock<IOutput> _mockOutput;

        public StringSalesPrinterTest()
        {
            _mockOutput = new Mock<IOutput>();
            _stringSalesPrinter = new StringSalesPrinter(_mockOutput.Object);
        }

        [Fact]
        void  when_list_order_contain_one_order_should_print_headers_and_one_order()
        {
            List<Order> orders = new List<Order>
            {
                new(1, "peter", 3, 123.0, DateTime.Parse("2021-11-30"))
            };

            List<string> headers = new List<string>
            {
                "orderid", "userName", "numberOfItems", "totalOfBasket", "dateOfBuy"
            };

            _stringSalesPrinter.Print(orders, headers);

            var expectedOutput =
                $@"+----------------------------------------------------------------------------------------------+
|          orderid |         userName |    numberOfItems |    totalOfBasket |        dateOfBuy |
+----------------------------------------------------------------------------------------------+
|                1 |            peter |                3 |           123.00 |       2021-11-30 |
+----------------------------------------------------------------------------------------------+";
            _mockOutput.Verify(output => output.Out(expectedOutput), Times.Once);
        }

        [Fact]
        void when_list_order_contain_few_orders_should_print_headers_and_all_orders()
        {
            List<Order> orders = new List<Order>
            {
                new(1, "peter", 3, 123.0, DateTime.Parse("2021-11-30")),
                new(2, "paul", 1, 433.50, DateTime.Parse("2021-12-11"))
            };

            List<string> headers = new List<string>
            {
                "orderid", "userName", "numberOfItems", "totalOfBasket", "dateOfBuy"
            };

            _stringSalesPrinter.Print(orders, headers);

            var expectedOutput =
                $@"+----------------------------------------------------------------------------------------------+
|          orderid |         userName |    numberOfItems |    totalOfBasket |        dateOfBuy |
+----------------------------------------------------------------------------------------------+
|                1 |            peter |                3 |           123.00 |       2021-11-30 |
|                2 |             paul |                1 |           433.50 |       2021-12-11 |
+----------------------------------------------------------------------------------------------+";
            _mockOutput.Verify(output => output.Out(expectedOutput), Times.Once);
        }
    }
}