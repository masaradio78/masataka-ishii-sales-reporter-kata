﻿##Sales report kata

###Context
This existing app reads a csv file, do some aggregations and calculations and then prints a clean and nice report about the results of the sales. The apps works great and has lots of comments explaining the details of the code!
so, what's the problem ?

###What's my job ?
(disclaimer : obviously if you have a simple app like that, that fits your needs it's probably good enough, but if you want to add new functionalities and make it richer over time, it won't)

discuss the app, 
- what's good & what's bad ?
  - The goods are that :
    - the program run
    - few variables names are correct
  - But the program have several bad parts
    - every instruction is in one file
    - and every instruction is in the main function
    - the visibility is not correct
    - few variables names can be improve to be readable like number1, number2...
    - big block of if and else
- discuss about the building blocks and hidden structure
  - 
- how should you test it ?
  - The golden master is already set so each change need verification by this big test
  - after the program is more readable we can start to move to other files certain behaviors and unit test those parts
- start refactoring & add tests