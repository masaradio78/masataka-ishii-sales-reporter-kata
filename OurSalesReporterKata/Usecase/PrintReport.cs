﻿using System.Collections.Generic;
using System.Linq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Parser;
using OurSalesReporterKata.Domain.Printer;
using OurSalesReporterKata.Usecase.interfaces;

namespace OurSalesReporterKata.Usecase
{
    public class PrintReport : IPrintReport
    {
        private readonly IReportPrinter _reportPrinter;
        private readonly IParserOrder _parserOrder;

        public PrintReport(IReportPrinter reportPrinter, IParserOrder parserOrder)
        {
            _reportPrinter = reportPrinter;
            _parserOrder = parserOrder;
        }

        public void Execute(string[] orderLines)
        {
            var orders = _parserOrder.ParseOrders(orderLines.Skip(1));
            var reportData = GetReportData(orders);
            
            _reportPrinter.Print(reportData);
        }

        private ReportData GetReportData(List<Order> orders)
        {
            return new ReportData(orders.Count, SumOfItems(orders), TotalSalesAmount(orders));
        }

        private static int SumOfItems(List<Order> orders)
        {
            return orders.Select(order => order.NumberOfItems).Sum();
        }
        
        private static double TotalSalesAmount(List<Order> orders)
        {
            return orders.Select(order => order.TotalOfBasket).Sum();
        }
    }
}