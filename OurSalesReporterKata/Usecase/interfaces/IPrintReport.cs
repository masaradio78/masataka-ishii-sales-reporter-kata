﻿namespace OurSalesReporterKata.Usecase.interfaces
{
    public interface IPrintReport
    {
        void Execute(string[] orderLines);
    }
}