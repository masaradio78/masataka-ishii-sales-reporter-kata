﻿namespace OurSalesReporterKata.Usecase.interfaces
{
    public interface IPrintSales
    {
        void Execute(string[] orderLines);
    }
}