﻿using System.Linq;
using OurSalesReporterKata.Domain.Parser;
using OurSalesReporterKata.Domain.Printer;
using OurSalesReporterKata.Usecase.interfaces;

namespace OurSalesReporterKata.Usecase
{
    public class PrintSales : IPrintSales
    {
        private readonly IParserOrder _parserOrder;
        private readonly ISalesPrinter _salesPrinter;

        public PrintSales(IParserOrder parserOrder, ISalesPrinter salesPrinter)
        {
            _parserOrder = parserOrder;
            _salesPrinter = salesPrinter;
        }

        public void Execute(string[] orderLines)
        {
            var contentLines = orderLines.Skip(1);
            var listHeaders = orderLines[0].Split(",").ToList();
            var orders = _parserOrder.ParseOrders(contentLines);
            _salesPrinter.Print(orders, listHeaders);
        }
    }
}