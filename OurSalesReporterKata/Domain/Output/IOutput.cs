﻿namespace OurSalesReporterKata.Domain.Output
{
    public interface IOutput
    {
        void Out(string message);
    }
}