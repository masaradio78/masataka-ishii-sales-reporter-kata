﻿using System.Collections.Generic;
using OurSalesReporterKata.Domain.Model;

namespace OurSalesReporterKata.Domain.Parser
{
    public interface IParserOrder
    {
        List<Order> ParseOrders(IEnumerable<string> lines);
    }
}