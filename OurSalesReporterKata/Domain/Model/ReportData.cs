﻿using System;

namespace OurSalesReporterKata.Domain.Model
{
    public class ReportData
    {
        public ReportData(int numberSales, int sumOfItems, double totalSalesAmount, int clientsCount = 0)
        {
            NumberSales = numberSales;
            SumOfItems = sumOfItems;
            TotalSalesAmount = totalSalesAmount;
            ClientsCount = clientsCount;
            AverageAmountSale = Math.Round(TotalSalesAmount / NumberSales, 2);
            AverageItemPrice = Math.Round(TotalSalesAmount / SumOfItems, 2);
        }

        public int NumberSales { get; }
        public int SumOfItems { get; }
        public double TotalSalesAmount { get; }
        public int ClientsCount { get; }
        public double AverageAmountSale { get; }
        public double AverageItemPrice { get; }

        private bool Equals(ReportData other)
        {
            return NumberSales == other.NumberSales && SumOfItems == other.SumOfItems &&
                   TotalSalesAmount.Equals(other.TotalSalesAmount) && ClientsCount == other.ClientsCount;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ReportData)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NumberSales, SumOfItems, TotalSalesAmount, ClientsCount);
        }
    }
}