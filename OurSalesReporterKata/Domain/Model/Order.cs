﻿using System;

namespace OurSalesReporterKata.Domain.Model
{
    public class Order
    {
        private readonly DateTime _dateOfBuy;

        public Order(int orderId, string userName, int numberOfItems, double totalOfBasket, DateTime dateOfBuy)
        {
            OrderId = orderId;
            UserName = userName;
            NumberOfItems = numberOfItems;
            TotalOfBasket = totalOfBasket;
            _dateOfBuy = dateOfBuy;
        }

        public int OrderId { get; }

        public string UserName { get; }

        public int NumberOfItems { get; }

        public double TotalOfBasket { get; }

        public DateTime DateOfBuy => _dateOfBuy;

        private bool Equals(Order other)
        {
            return OrderId == other.OrderId && UserName == other.UserName &&
                   NumberOfItems == other.NumberOfItems && TotalOfBasket.Equals(other.TotalOfBasket) &&
                   _dateOfBuy.Equals(other._dateOfBuy);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Order)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(OrderId, UserName, NumberOfItems, TotalOfBasket, _dateOfBuy);
        }
    }
}