﻿using OurSalesReporterKata.Domain.Model;

namespace OurSalesReporterKata.Domain.Printer
{
    public interface IReportPrinter
    {
        void Print(ReportData data);
    }
}