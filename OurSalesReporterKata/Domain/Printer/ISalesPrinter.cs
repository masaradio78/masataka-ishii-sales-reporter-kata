﻿using System.Collections.Generic;
using OurSalesReporterKata.Domain.Model;

namespace OurSalesReporterKata.Domain.Printer
{
    public interface ISalesPrinter
    {
        void Print(List<Order> orders, List<string> headers);
    }
}