﻿namespace OurSalesReporterKata.Domain.Printer
{
    public interface IShowCommandPrinter
    {
        void Print();
    }
}