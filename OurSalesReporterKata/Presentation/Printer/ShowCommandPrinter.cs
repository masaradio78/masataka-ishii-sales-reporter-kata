﻿using OurSalesReporterKata.Domain.Output;
using OurSalesReporterKata.Domain.Printer;
using OurSalesReporterKata.Usecase.interfaces;

namespace OurSalesReporterKata.Presentation.Printer
{
    public class ShowCommandPrinter: IShowCommandPrinter
    {
        private readonly IOutput _output;

        public ShowCommandPrinter(IOutput output)
        {
            _output = output;
        }

        public void Print()
        {
            _output.Out("[ERR] your command is not valid ");
            _output.Out("Help: ");
            _output.Out("    - [print]  : show the content of our commerce records in data.csv");
            _output.Out("    - [report] : show a summary from data.csv records ");
        }
    }
}