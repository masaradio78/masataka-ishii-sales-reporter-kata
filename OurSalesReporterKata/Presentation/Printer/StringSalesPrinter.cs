﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Output;
using OurSalesReporterKata.Domain.Printer;

namespace OurSalesReporterKata.Presentation.Printer
{
    public class StringSalesPrinter : ISalesPrinter
    {
        private readonly IOutput _output;
        private const char HorizontalBarChar = '-';
        private const int SpaceBetweenBarAndValue = 16;
        private const string TableCorner = "+";
        private const string BarColumn = " | ";

        public StringSalesPrinter(IOutput output)
        {
            _output = output;
        }

        public void Print(List<Order> orders, List<string> headers)
        {
            var headersResult = GetHeadersResult(headers);
            _output.Out(SalesResult(orders, headersResult));
        }

        private static string GetHeadersResult(List<string> headers) =>
            @$"{string.Join(
                BarColumn,
                headers.Select(header => header.PadLeft(SpaceBetweenBarAndValue))
            )}";

        private string SalesResult(List<Order> orders, string headersResult) =>
            @$"{HorizontalBar(headersResult.Length + 2)}
| {headersResult} |
{HorizontalBar(headersResult.Length + 2)}
{ConvertOrdersToStringSalesContent(orders)}
{HorizontalBar(headersResult.Length + 2)}";

        private string HorizontalBar(int lengthWithoutCorner) =>
            TableCorner + new string(HorizontalBarChar, lengthWithoutCorner) + TableCorner;

        private string ConvertOrdersToStringSalesContent(List<Order> orders) => string
            .Join(Environment.NewLine, orders
                .Select(ConvertOneOrderToStringSaleContent)
            );

        private string ConvertOneOrderToStringSaleContent(Order order)
        {
            var result = $"| {order.OrderId.ToString(),SpaceBetweenBarAndValue} | ";
            result += $"{order.UserName,SpaceBetweenBarAndValue} | ";
            result += $"{order.NumberOfItems.ToString(),SpaceBetweenBarAndValue} | ";
            result +=
                $@"{Math.Round(order.TotalOfBasket, 2)
                    .ToString(".00", CultureInfo.InvariantCulture)
                    .ToString(CultureInfo.InvariantCulture),SpaceBetweenBarAndValue} | ";
            result += $"{order.DateOfBuy,SpaceBetweenBarAndValue:yyyy-MM-dd} |";
            return result;
        }
    }
}