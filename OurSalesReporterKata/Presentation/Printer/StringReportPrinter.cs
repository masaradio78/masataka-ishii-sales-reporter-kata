﻿using System;
using System.Globalization;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Output;
using OurSalesReporterKata.Domain.Printer;

namespace OurSalesReporterKata.Presentation.Printer
{
    public class StringReportPrinter : IReportPrinter
    {
        private readonly IOutput _output;

        public StringReportPrinter(IOutput output)
        {
            _output = output;
        }

        public void Print(ReportData data)
        {
            var reportResult = GetReportResult(data.NumberSales, data.SumOfItems, data.TotalSalesAmount,
                data.ClientsCount,
                data.AverageAmountSale, data.AverageItemPrice);
            _output.Out(reportResult);
        }

        private static string GetReportResult(int numberSales, int sumOfItems, double totalSalesAmount,
            int clientsCount,
            double averageAmountSale, double averageItemPrice)
        {
            return @$"+{new string('-', 45)}+
| {" Number of sales",30} | {numberSales.ToString(),10} |
| {" Number of clients",30} | {clientsCount.ToString(),10} |
| {" Total items sold",30} | {sumOfItems.ToString(),10} |
| {" Total sales amount",30} | {Math.Round(totalSalesAmount, 2).ToString(CultureInfo.InvariantCulture),10} |
| {" Average amount/sale",30} | {averageAmountSale.ToString(CultureInfo.InvariantCulture),10} |
| {" Average item price",30} | {averageItemPrice.ToString(CultureInfo.InvariantCulture),10} |
+{new string('-', 45)}+";
        }
    }
}