﻿using System.IO;
using OurSalesReporterKata.Domain.Output;
using OurSalesReporterKata.Domain.Parser;
using OurSalesReporterKata.Domain.Printer;
using OurSalesReporterKata.Infrastructure.Output;
using OurSalesReporterKata.Infrastructure.Parser;
using OurSalesReporterKata.Presentation.Printer;
using OurSalesReporterKata.Usecase;
using OurSalesReporterKata.Usecase.interfaces;

namespace OurSalesReporterKata
{
    public static class OurProgram
    {
        public static void Main(string[] args)
        {
            IOutput output = new ConsoleOutput();
            IShowCommandPrinter showCommandPrinter = new ShowCommandPrinter(output);
            IReportPrinter reportPrinter = new StringReportPrinter(output);
            ISalesPrinter salesPrinter = new StringSalesPrinter(output);
            IParserOrder parserOrder = new CsvParserOrder();
            IPrintSales printSales = new PrintSales(parserOrder, salesPrinter);
            IPrintReport printReport = new PrintReport(reportPrinter, parserOrder);
            
            string command = args.Length > 0 ? args[0] : "unknown";
            string file = args.Length >= 2 ? args[1] : "./data.csv";
            string[] linesFileContent = File.ReadAllLines(file);

            output.Out("=== Sales Viewer ===");
            switch (command)
            {
                case "print":
                    printSales.Execute(linesFileContent);
                    break;
                case "report":
                    printReport.Execute(linesFileContent);
                    break;
                default:
                    showCommandPrinter.Print();
                    break;
            }
        }
    }
}