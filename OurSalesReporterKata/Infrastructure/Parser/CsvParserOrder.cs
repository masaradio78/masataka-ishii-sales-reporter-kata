﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OurSalesReporterKata.Domain.Model;
using OurSalesReporterKata.Domain.Parser;

namespace OurSalesReporterKata.Infrastructure.Parser
{
    public class CsvParserOrder: IParserOrder
    {
        public List<Order> ParseOrders(IEnumerable<string> lines)
        {
            return lines.Select(GetNewOrderByLine).ToList();
        }

        private static Order GetNewOrderByLine(string line)
        {
            var lineContent = line.Split(", ");
            var orderId = int.Parse(lineContent[0]);
            var userName = lineContent[1];
            var numberOfItems = int.Parse(lineContent[2]);
            var totalOfBasket = double.Parse(lineContent[3], CultureInfo.InvariantCulture);
            var dateTime = DateTime.Parse(lineContent[4]);
            return new Order(orderId, userName, numberOfItems, totalOfBasket, dateTime);
        }
    }
}