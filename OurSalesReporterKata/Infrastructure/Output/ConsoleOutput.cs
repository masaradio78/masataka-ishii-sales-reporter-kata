﻿using System;
using OurSalesReporterKata.Domain.Output;

namespace OurSalesReporterKata.Infrastructure.Output
{
    public class ConsoleOutput: IOutput
    {
        public void Out(string message)
        {
            Console.WriteLine(message);
        }
    }
}